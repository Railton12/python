#Importando Bibliotecas
from tkinter import*
import sqlite3
#Definindo classe
class Project:
    #Definindo Inicialização
    def __init__(self, root,):
        self.root= root
        self.root.title("Projeto")
        #Criando Variaveis
        EMAIL = StringVar()
        SENHA = StringVar()
        #Criando Frame
        frame = LabelFrame(self.root, bg="Purple")
        frame.place(x=61, y=200)
        #Criando Labels
        lbl_email = Label(frame, text="Email", bg="Purple")
        lbl_email.grid(row=1, column=1)
        lbl_senha = Label(frame, text="Senha", bg="Purple")
        lbl_senha.grid(row=2, column=1)
        lbl_aviso = Label(frame, bg="Purple")
        lbl_aviso.grid(row=4, column=2)
        #Criando os Campos de Digitar
        camp_email = Entry(frame, textvariable=EMAIL)
        camp_email.grid(row=1, column=2)
        camp_senha = Entry(frame, textvariable=SENHA, show="*")
        camp_senha.grid(row=2, column=2)
        # Criando Banco de Dados
        def Database():
            global conn, cursor
            conn = sqlite3.connect("DataPB.db")
            cursor = conn.cursor()
            cursor.execute(
                "CREATE TABLE IF NOT EXISTS `Usuarios` (idUsuarios INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, nome TEXT, cpf TEXT, email TEXT, senha TEXT)")
            cursor.execute("SELECT * FROM `Usuarios` WHERE `email` = 'Ita28' AND `senha` = 'Ita28'")
            if cursor.fetchone() is None:
                cursor.execute("INSERT INTO `Usuarios` (email, senha) VALUES('Ita28', 'Ita28')")
                conn.commit()
        # Criando Condição de Login
        def Conectar(event=None):
                Database()
                if EMAIL.get() == "" or SENHA.get == "":
                    lbl_aviso.config(text="Por favor digite o email e senha", fg="black")
                else:
                    cursor.execute("SELECT * FROM `Usuarios` WHERE `email` = ? AND `senha` = ?",(EMAIL.get(), SENHA.get()))
                    if cursor.fetchone() is not None:
                        Homewindow()
                        EMAIL.set("")
                        SENHA.set("")
                        lbl_aviso.config(text="")
                    else:
                        lbl_aviso.config(text="Senha errada", fg="red")
                        EMAIL.set("")
                        SENHA.set("")
                cursor.close()
                conn.close()
        # Criando Botões de command de login
        btn_login = Button(frame, text="Conectar", width=9, command=Conectar)
        btn_login.grid(row=3, column=1)
        btn_login.bind('<Return>', Conectar)
        btn_cadastra = Button(frame, text="Cadastrar", width=9)
        btn_cadastra.grid(row=3, column=2)
        # Criando Uma Sengunda Tela Apos Confirmação do Login
        def Homewindow():
            global Home
            root.withdraw()
            Home = Toplevel()
            Home.title("Bem Vindo")
            width = 320
            height = 480
            screen_width = root.winfo_screenwidth()
            screen_height = root.winfo_screenheight()
            x = (screen_width / 2) - (width / 2)
            y = (screen_height / 2) - (height / 2)
            root.resizable(0, 0)
            Home.geometry("%dx%d+%d+%d" % (width, height, x, y))

if __name__ == "__main__":
    root = Tk()
    root["bg"] = "Purple"
    root.title("Projeto")
    Imagem = PhotoImage(file="te.png")
    Logo = Label(root, image=Imagem, bg="Purple")
    Logo.place(x=72, y=0)
    width = 320
    height = 480
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    x = (screen_width / 2) - (width / 2)
    y = (screen_height / 2) - (height / 2)
    root.geometry("%dx%d+%d+%d" % (width, height, x, y))
    root.resizable(0, 0)
    application = Project(root)
    root.mainloop()